from keras.models import Sequential
from keras.layers.core import Dense,Activation
import pandas as pd
import theano
import numpy as np
import xlsxwriter


my_model = Sequential()
print('Loading data...')
training_data_x = pd.read_excel("/home/rianzaman/Downloads/New_Datas/50_agents/inputs/training_data_for_x.xlsx", header = None)
training_data_y = pd.read_excel("/home/rianzaman/Downloads/New_Datas/50_agents/inputs/training_data_for_y.xlsx", header = None)
test_data_x = pd.read_excel("/home/rianzaman/Downloads/New_Datas/50_agents/inputs/test_data_for_x.xlsx", header = None)
test_data_y = pd.read_excel("/home/rianzaman/Downloads/New_Datas/50_agents/inputs/test_data_for_y.xlsx", header = None)

X_train = training_data_x.as_matrix(columns=None)
Y_train = training_data_y.as_matrix(columns=None)

X_test = test_data_x.as_matrix(columns=None)
Y_test = test_data_y.as_matrix(columns=None)

size_of_batch = 100
number_of_epoch =200

print('Building model...')
########################################################################
my_model.add(Dense(100, input_shape=(49,), activation='relu'))
my_model.add(Dense(100,activation = "relu"))
my_model.add(Dense(9, activation="softmax"))
########################################################################

print('Configuring the model...')
my_model.compile(loss = 'categorical_crossentropy', optimizer = 'adadelta')

print('Training the network...')
my_model.fit(X_train, Y_train,batch_size=size_of_batch, nb_epoch=number_of_epoch, show_accuracy=False, verbose=1,
         validation_data=(X_test, Y_test))

print('Saving weights...')
my_model.save_weights( '/home/rianzaman/Downloads/New_Datas/50_agents/outputs/layer_weights.h5')
'''
print('Loading Weights...')
my_model.load_weights( '/home/rianzaman/Downloads/New_Datas/50_agents/outputs/layer_weights.h5')
'''

'''
mean_absolute_error
categorical_crossentropy
49,100
100,100
100,9
'''

#Weight Visualization
print("Printing Weights of layer...")
weights_1 = my_model.layers[0].get_weights()
weights_2 = my_model.layers[1].get_weights()
weights_3 = my_model.layers[2].get_weights()
reshpaped_weights = np.reshape(weights_1[0], (49,100))
write_weights = pd.ExcelWriter("/home/rianzaman/Downloads/New_Datas/50_agents/outputs/weights_1.xlsx",engine="xlsxwriter")
weights_output = pd.DataFrame(reshpaped_weights)
weights_output.to_excel(write_weights, sheet_name='Sheet1',index = False)

reshpaped_weights_1 = np.reshape(weights_2[0], (100,100))
write_weights_1 = pd.ExcelWriter("/home/rianzaman/Downloads/New_Datas/50_agents/outputs/weights_2.xlsx",engine="xlsxwriter")
weights_output_1 = pd.DataFrame(reshpaped_weights_1)
weights_output_1.to_excel(write_weights_1, sheet_name='Sheet1',index = False)

reshpaped_weights_2 = np.reshape(weights_3[0], (100,9))
write_weights_2 = pd.ExcelWriter("/home/rianzaman/Downloads/New_Datas/50_agents/outputs/weights_3.xlsx",engine="xlsxwriter")
weights_output_2 = pd.DataFrame(reshpaped_weights_2)
weights_output_2.to_excel(write_weights_2, sheet_name='Sheet1',index = False)

#Weight visualization ends.

#Bias weights starts:

bias_1 = np.reshape(weights_1[1], (1,100))
bias_2 = np.reshape(weights_2[1], (1,100))
bias_3 = np.reshape(weights_3[1], (1,9))

write_bias_1 = pd.ExcelWriter("/home/rianzaman/Downloads/New_Datas/50_agents/outputs/bias_1.xlsx",engine="xlsxwriter")
bias_1_output = pd.DataFrame(bias_1)
bias_1_output.to_excel(write_bias_1, sheet_name='Sheet1',index = False)

write_bias_2 = pd.ExcelWriter("/home/rianzaman/Downloads/New_Datas/50_agents/outputs/bias_2.xlsx",engine="xlsxwriter")
bias_2_output = pd.DataFrame(bias_2)
bias_2_output.to_excel(write_bias_2, sheet_name='Sheet1',index = False)

write_bias_3 = pd.ExcelWriter("/home/rianzaman/Downloads/New_Datas/50_agents/outputs/bias_3.xlsx",engine="xlsxwriter")
bias_3_output = pd.DataFrame(bias_3)
bias_3_output.to_excel(write_bias_3, sheet_name='Sheet1',index = False)
#Bias weights ends here.

score = my_model.evaluate(X_test, Y_test, show_accuracy=False, verbose=0)
