from keras.models import Sequential
from keras.layers.core import Dense
import pandas as pd
import numpy as np

my_model = Sequential()

training_data_x = pd.read_excel("/home/rianzaman/Downloads/input_v2.xlsx", header = None)
training_data_y = pd.read_excel("/home/rianzaman/Downloads/output_v2.xlsx", header = None)
test_data_x = pd.read_excel("/home/rianzaman/Downloads/test_data_for_x.xlsx", header = None)
test_data_y = pd.read_excel("/home/rianzaman/Downloads/test_data_for_y.xlsx", header = None)
size_of_batch = 100
number_of_epoch = 20

X_train = training_data_x.as_matrix(columns=None)
Y_train = training_data_y.as_matrix(columns=None)

X_test = test_data_x.as_matrix(columns=None)
Y_test = test_data_y.as_matrix(columns=None)

my_model.add(Dense(150,input_shape=(90,)))
my_model.add(Dense(90,))
my_model.compile(loss = 'mean_absolute_error', optimizer = 'adadelta')

my_model.fit(X_train, Y_train,batch_size=size_of_batch, nb_epoch=number_of_epoch, show_accuracy=False, verbose=2,
          validation_data=(X_test, Y_test))

score = my_model.evaluate(X_test, Y_test, show_accuracy=False, verbose=0)
#print('Test score:', score[0])
#print('Test accuracy:', score[1])
