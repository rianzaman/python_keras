import pandas as pd
import numpy as np

matrix_array = np.zeros((7, 7))
matrix_array[3][4] = 1
print(matrix_array)
'''
file = open("/home/rianzaman/Downloads/New_Datas/30_agents/2_hidden_layers/netlogo.txt", "w")

for i in range (300):
    #file.write("[who] of turtle " + str(i)+ "\n")
    file.write("[heading] of turtle " + str(i)+ "\n")
    file.write("[xcor] of turtle  " + str(i)+ "\n")
    file.write("[ycor] of turtle " + str(i)+ "\n\n")
file.close()

netlogo_data = pd.read_excel("/home/rianzaman/Downloads/New_Datas/30_agents/2_hidden_layers/inputs/netlogo_data.xlsx", header = None)
netlogo_array = netlogo_data.as_matrix(columns=None)

#Deleting a specific column:
number_of_column_to_delete = []
predict_arr_col = 90
for cl in range(predict_arr_col):
    if(cl%3 is 0):
        number_of_column_to_delete.append(cl)
test_array_container = np.delete(netlogo_array,number_of_column_to_delete, axis = 1)

write_prediction_after_delete = pd.ExcelWriter("/home/rianzaman/Downloads/New_Datas/30_agents/2_hidden_layers/controlled_non_lin_out/netlogo_array_after_delete.xlsx",engine="xlsxwriter")
output_prediction_after_delete_data = pd.DataFrame(test_array_container)
output_prediction_after_delete_data.to_excel(write_prediction_after_delete, sheet_name='Sheet1',index = False)
#Deleting a specific column code ends here.

#Getting Datas from an array
temp_x_list = []
temp_y_list = []

write_data_of_xy = pd.ExcelWriter("/home/rianzaman/Downloads/New_Datas/30_agents/2_hidden_layers/controlled_non_lin_out/netlogo_xy_data.xlsx",engine="xlsxwriter")
new_rows = 200
new_columns = 60
excel_col_x = 1
excel_col_y = 2
for i in range(0 , new_rows):
    #print("Doing something .....")
    temp_x_list = []
    temp_y_list = []
    for j in range(0,new_columns):
        if(j%2 == 0):
            #print("Working on X...")
            temp_x_list.append(test_array_container[i][j])
        else:
            #print("Working on Y...")
            temp_y_list.append(test_array_container[i][j])
        j+=1
    i+=1
    output_x = pd.DataFrame(temp_x_list)
    output_x.to_excel(write_data_of_xy, sheet_name='Sheet1',startcol=excel_col_x ,index = False)
    output_y = pd.DataFrame(temp_y_list)
    output_y.to_excel(write_data_of_xy, sheet_name='Sheet1',startcol=excel_col_y, index = False)
    excel_col_x +=3
    excel_col_y +=3

'''