import pandas as pd
import numpy as np
from math import *

print('Loading data...')
agents_initial_data = pd.read_excel("/home/rianzaman/Downloads/New_Datas/50_agents/inputs/agent_position_5.xlsx", header = None)
agents_initial = agents_initial_data.as_matrix(columns=None)
#Deleting a specific column:
number_of_column_to_delete = []
arr_col = 150
for cl in range(arr_col):
    if(cl%3 is 0):
        number_of_column_to_delete.append(cl)
arr_container = np.delete(agents_initial,number_of_column_to_delete, axis = 1)
# write_prediction_after_delete = pd.ExcelWriter("/home/rianzaman/Downloads/New_Datas/50_agents/outputs/initial_position_after_delete.xlsx",engine="xlsxwriter")
# output_prediction_after_delete_data = pd.DataFrame(arr_container)
# output_prediction_after_delete_data.to_excel(write_prediction_after_delete, sheet_name='Sheet1',header = False,index = False)
#Deleting a specific column code ends here.

xy_data = pd.ExcelWriter(
    "/home/rianzaman/Downloads/New_Datas/50_agents/outputs/agents_neighbor_metrix_5.xlsx",
    engine="xlsxwriter")
input_data_x = pd.ExcelWriter(
    "/home/rianzaman/Downloads/New_Datas/50_agents/outputs/x_data_5.xlsx",
    engine="xlsxwriter")

row_num = 0
input_col = 0
for index_i in range(0,999):
    ind_x = 0
    ind_y = 1
    for i in range(0, 50):
        comp_x = 0
        comp_y = 1
        temp_array = []
        input_arr = []
        temp_array.append(arr_container[index_i][ind_x])
        temp_array.append(arr_container[index_i][ind_y])
        print("Current value being compared with: ", temp_array)
        print(temp_array[0])
        print(temp_array[1])
        lst = [3]

        # file = open("/home/rianzaman/Downloads/New_Datas/300_agents/outputs/agents_location/test_" + str(i) + ".txt", "w")
        matrix_array = np.zeros((7, 7))
        # matrix_array[3][3] = 1
        #print("initial matrix array")
        #print(matrix_array)
        for trav in range(0, 50):
            ind = []
            compare_arr = []
            compare_arr.append(arr_container[index_i][comp_x])
            compare_arr.append(arr_container[index_i][comp_y])
            # print("Current compare value",compare_arr)
            temp_x = temp_array[0] - compare_arr[0]
            temp_y = temp_array[1] - compare_arr[1]
            if(abs(temp_x) > 68 and abs(temp_y) < 68):
                temp_array[0] = abs(temp_array[0])
                compare_arr[0] = abs(compare_arr[0])

            elif(abs(temp_x) < 68 and abs(temp_y) > 68):
                temp_array[1] = abs(temp_array[1])
                compare_arr[1] = abs(compare_arr[1])
            elif(abs(temp_x) > 68 and abs(temp_y) > 68):
                temp_array[0] = abs(temp_array[0])
                compare_arr[0] = abs(compare_arr[0])
                temp_array[1] = abs(temp_array[1])
                compare_arr[1] = abs(compare_arr[1])

            if (((compare_arr[0] > temp_array[0] and compare_arr[0] <= (temp_array[0] + lst[0]))
                 or (compare_arr[0] < temp_array[0] and compare_arr[0] >= (temp_array[0] - lst[0])))
                and ((compare_arr[1] > temp_array[1] and compare_arr[1] <= (temp_array[1] + lst[0]))
                     or (compare_arr[1] < temp_array[1] and compare_arr[1] >= (temp_array[1] - lst[0])))):
                ind.append(temp_array[0] - compare_arr[0])
                ind.append(temp_array[1] - compare_arr[1])
                print("printing ind value")
                print(ind[0])
                print(ind[1])
                print("Rounded number:")
                print(round(ind[0]))
                print(round(ind[1]))
                if(matrix_array[3 + round(ind[1])][3 + round(ind[0])] != 0):
                    matrix_array[3 + round(ind[1])][3 + round(ind[0])] +=1

                matrix_array[3 + round(ind[1])][3 - round(ind[0])] = 1
            # print(ind, trav)

            comp_x += 2
            comp_y += 2
        print("Final matrix array")
        print(matrix_array)
        print("Loading the M4A wait a bit... Reloded ", index_i,"Times.")
        output = pd.DataFrame(matrix_array)
        output.to_excel(xy_data, startrow=row_num, index=False, header=False)

        for in_arr in range(7):
            for in_arra in range(7):
                input_arr.append(matrix_array[in_arr][in_arra])
        print("#############################")
        print(input_arr)
        print("#############################")

        reshape = np.reshape(input_arr, (1, 49))
        output_data = pd.DataFrame(reshape)
        output_data.to_excel(input_data_x, startrow=input_col, index=False, header=False)
        print("#############################")
        print(reshape)
        print("#############################")
        ind_x += 2
        ind_y += 2
        row_num += 8
        input_col += 1


out_data = pd.ExcelWriter(
    "/home/rianzaman/Downloads/New_Datas/50_agents/outputs/y_data_5.xlsx",
    engine="xlsxwriter")
in_col = 0
count = 0
for still in range (0,999):
    num_x = 0
    num_y = 1
    for travel in range(50):
        xy_list = []
        final_out_arr = []
        i = 0
        j = 0
        print("printing still",still,count)
        output_arr = np.zeros((3, 3))
        #output_arr[1][1] = 1
        xy_list.append(arr_container[still][num_x])
        xy_list.append(arr_container[still][num_y])
        xy_list.append(arr_container[still + 1][num_x])
        xy_list.append(arr_container[still + 1][num_y])
        print(xy_list[0])
        print(xy_list[1])
        print(xy_list[2])
        print(xy_list[3])
        i = round(xy_list[0] - xy_list[2])
        j = round(xy_list[1] - xy_list[3])
        if(i > 10 or i < -2):
            i = round(xy_list[0] + xy_list[2])
        if(j > 10 or j < -2):
            j = round(xy_list[1] + xy_list[3])
        if (i > 1):
             i = 1
        elif(i < -1):
            i = -1
        if (j > 1 ):
             j = 1
        elif(j < -1):
            j = -1


        output_arr[1 + i][1 + j] = 1
        print(output_arr)

        for x in range(3):
            for xx in range(3):
                final_out_arr.append(output_arr[x][xx])
        print("#############################")
        print(final_out_arr)
        print("#############################")

        reshaped_out = np.reshape(final_out_arr, (1, 9))
        fin_out_data = pd.DataFrame(reshaped_out)
        fin_out_data.to_excel(out_data, startrow=in_col, index=False, header=False)

        num_x += 2
        num_y += 2
        in_col +=1

"""

25000, 49 array

25000 axis random permutation

indexes =np.random.permute( 25000)

useable_Array = your_current_array[ indexes ]

useable_Array --? split in 23,000 and 2,000



"""


'''
for i in range(0,60):
    agent_center = arr_container[index_i][i]
    print("#############################")
    print(agent_center)
    print("#############################")
    temp_list_x = []
    temp_list_y = []
    if(i%2 == 0):
        for trav in range(0,60,2):
            if((arr_container[index_i][trav] > agent_center and arr_container[index_i][trav] <= (agent_center+3))
               or (arr_container[index_i][trav] < agent_center and arr_container[index_i][trav] >= (agent_center-3))):
                temp_list_x.append(arr_container[index_i][trav])
                temp_list_x.append("x")

    else:
        for trav in range(1, 60, 2):
            if ((arr_container[index_i][trav] > agent_center and arr_container[index_i][trav] <= (agent_center + 3))
                or (arr_container[index_i][trav] < agent_center and arr_container[index_i][trav] >= (agent_center - 3))):
                temp_list_y.append(arr_container[index_i][trav])
                temp_list_y.append("y")
    xy_data = pd.ExcelWriter(
        "/home/rianzaman/Downloads/New_Datas/30_agents/2_hidden_layers/initial_output/data_agents_"+str(i)+".xlsx",
        engine="xlsxwriter")
    output_x = pd.DataFrame(temp_list_x)
    output_x.to_excel(xy_data, sheet_name='Sheet1', startcol=col_x, index=False)
    output_y = pd.DataFrame(temp_list_y)
    output_y.to_excel(xy_data, sheet_name='Sheet1', startcol=col_y, index=False)
    col_x += 3
    col_y += 3
'''
'''
for i in range(1,60,2):
    agent_center = arr_container[index_i][i]
    print("#############################")
    print(agent_center)
    print("#############################")
    temp_list = []
    for trav in range(1,60,2):
        if(arr_container[index_i][trav] > agent_center and arr_container[index_i][trav] <= (agent_center+3)):
            temp_list.append(arr_container[index_i][trav])
            y_data = pd.ExcelWriter(
                "/home/rianzaman/Downloads/New_Datas/30_agents/2_hidden_layers/initial_output/y_data_agent_"+str(i)+" .xlsx",
                engine="xlsxwriter")
            agents_y = pd.DataFrame(temp_list)
            agents_y.to_excel(y_data, sheet_name='Sheet1',
                                                         index=False)
'''