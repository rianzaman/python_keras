import pandas as pd
import numpy as np

print('Loading data...')
agents_data = pd.read_excel("/home/rianzaman/Downloads/New_Datas/50_agents/inputs/x_data.xlsx", header = None)
agents_initial_x = agents_data.as_matrix(columns=None)

agents_data_y = pd.read_excel("/home/rianzaman/Downloads/New_Datas/50_agents/inputs/y_data.xlsx", header = None)
agents_initial_y = agents_data_y.as_matrix(columns=None)

indexes = np.random.permutation(249750)

new_training_arr = agents_initial_x[indexes]
new_training_arr_y = agents_initial_y[indexes]
print("Saving Data...")
indexes_data = pd.ExcelWriter(
    "/home/rianzaman/Downloads/New_Datas/50_agents/outputs/indexes.xlsx",
    engine="xlsxwriter")
index_output = pd.DataFrame(indexes)
index_output.to_excel(indexes_data, index=False, header=False)

print("Saving Data...")
training_data = pd.ExcelWriter(
    "/home/rianzaman/Downloads/New_Datas/50_agents/outputs/new_input_data.xlsx",
    engine="xlsxwriter")
output_data = pd.DataFrame(new_training_arr)
output_data.to_excel(training_data, index=False, header=False)


print("Saving Data...")
training_data_y = pd.ExcelWriter(
    "/home/rianzaman/Downloads/New_Datas/50_agents/outputs/new_output_data.xlsx",
    engine="xlsxwriter")
output_data_y = pd.DataFrame(new_training_arr_y)
output_data_y.to_excel(training_data_y, index=False, header=False)




"""

array = [0,1,2,3,4]
indexes = [2,3,1,0]

array[indexes]
"""