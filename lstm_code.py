from keras.models import Sequential
from keras.layers.core import Dense, Activation
import pandas as pd
from keras.layers.recurrent import LSTM
from keras.layers.embeddings import Embedding
import numpy as np

my_model = Sequential()

training_data_x = pd.read_excel("/home/rianzaman/Downloads/new_input_v1.xlsx", header = None)
training_data_y = pd.read_excel("/home/rianzaman/Downloads/new_output_v1.xlsx", header = None)
test_data_x = pd.read_excel("/home/rianzaman/Downloads/new_test_data_for_x.xlsx", header = None)
test_data_y = pd.read_excel("/home/rianzaman/Downloads/new_test_data_for_y.xlsx", header = None)
size_of_batch = 100
number_of_epoch = 20
print('Loading data...')
X_train = training_data_x.as_matrix(columns=None)
Y_train = training_data_y.as_matrix(columns=None)

X_test = test_data_x.as_matrix(columns=None)
Y_test = test_data_y.as_matrix(columns=None)

print('Building model...')
my_model.add(LSTM(90,150,))
my_model.add(Dense(90,150))


my_model.compile(loss = 'mean_squared_error', optimizer = 'adadelta')
print('Training the network...')
my_model.fit(X_train, Y_train,batch_size=size_of_batch, nb_epoch=number_of_epoch, show_accuracy=False, verbose=2,
          validation_data=(X_test, Y_test))

score = my_model.evaluate(X_test, Y_test, show_accuracy=False, verbose=0)
#print('Test score:', score[0])
#print('Test accuracy:', score[1])
