from keras.models import Sequential
from keras.layers.core import Dense
import pandas as pd
import theano
import numpy as np
import xlsxwriter

my_model = Sequential()

training_data_x = pd.read_excel("/home/rianzaman/Downloads/new_input_v1.xlsx", header = None)
training_data_y = pd.read_excel("/home/rianzaman/Downloads/new_output_v1.xlsx", header = None)
test_data_x = pd.read_excel("/home/rianzaman/Downloads/new_test_data_for_x.xlsx", header = None)
test_data_y = pd.read_excel("/home/rianzaman/Downloads/new_test_data_for_y.xlsx", header = None)

size_of_batch = 1
number_of_epoch =200

print('Loading data...')
X_train = training_data_x.as_matrix(columns=None)
Y_train = training_data_y.as_matrix(columns=None)

X_test = test_data_x.as_matrix(columns=None)
Y_test = test_data_y.as_matrix(columns=None)

print('Building model...')

my_model.add(Dense(150,input_shape=(90,)))
my_model.add(Dense(90,))

my_model.compile(loss = 'mean_absolute_error', optimizer = 'adadelta')
print('Training the network...')

my_model.fit(X_train, Y_train,batch_size=size_of_batch, nb_epoch=number_of_epoch, show_accuracy=False, verbose=2,
          validation_data=(X_test, Y_test))

write_predict_data = pd.ExcelWriter("/home/rianzaman/Downloads/prediction_data_2.xlsx",engine="xlsxwriter")
write_activations_data = pd.ExcelWriter("/home/rianzaman/Downloads/activition_of_hidden_node_2.xlsx",engine="xlsxwriter")
for i in range(0, 200):
    print("Predicting ...",)
    next_prediction = my_model.predict(X_test, 1,)
    output_file_data = pd.DataFrame(next_prediction)
    output_file_data.to_excel(write_predict_data, sheet_name='Sheet1')

    X_test = next_prediction

write_predict_data.save()

# To get activation
get_activations = theano.function([my_model.layers[0].input], my_model.layers[0].get_output(train=False),
                                  allow_input_downcast=True)
activations = get_activations(X_test)
output_file_data_activation = pd.DataFrame(activations)
output_file_data_activation.to_excel(write_activations_data, sheet_name='Sheet1')


score = my_model.evaluate(X_test, Y_test, show_accuracy=False, verbose=0)


#print('Test score:', score[0])
#print('Test accuracy:', score[1])

