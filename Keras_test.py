from keras.models import Sequential
from keras.layers.core import Dense
import pandas as pd
import csv
import theano
import numpy as np
import xlsxwriter

my_model = Sequential()

training_data_x = pd.read_excel("/home/rianzaman/Downloads/new_input_v1.xlsx", header = None)
training_data_y = pd.read_excel("/home/rianzaman/Downloads/new_output_v1.xlsx", header = None)
test_data_x = pd.read_excel("/home/rianzaman/Downloads/new_test_data_for_x.xlsx", header = None)
test_data_y = pd.read_excel("/home/rianzaman/Downloads/new_test_data_for_y.xlsx", header = None)

size_of_batch = 100
number_of_epoch =2

print('Loading data...')
X_train = training_data_x.as_matrix(columns=None)
Y_train = training_data_y.as_matrix(columns=None)

X_test = test_data_x.as_matrix(columns=None)
Y_test = test_data_y.as_matrix(columns=None)

print('Building model...')

my_model.add(Dense(150,input_shape=(90,)))
my_model.add(Dense(90,))

my_model.compile(loss = 'mean_absolute_error', optimizer = 'adadelta')
print('Training the network...')

my_model.fit(X_train, Y_train,batch_size=size_of_batch, nb_epoch=number_of_epoch, show_accuracy=False, verbose=2,
         validation_data=(X_test, Y_test))


#my_model.save_weights( './some_weights.h5')

#my_model.load_weights( './some_weights.h5')

write_predict_data = pd.ExcelWriter("/home/rianzaman/Downloads/prediction_data_test.xlsx",engine="xlsxwriter")
write_activations_data = pd.ExcelWriter("/home/rianzaman/Downloads/activition_of_hidden_node_test.xlsx",engine="xlsxwriter")

get_activations = theano.function([my_model.layers[0].input], my_model.layers[0].get_output(train=False),
                                  allow_input_downcast=True)

sliced = X_test[0:1,:]
activation_list = []
prediction_list = []
for i in range(0, 200):
    #print("Sliced dim:")
    #print(sliced.shape)
    print("Predicting ...")
    next_prediction = my_model.predict(sliced, 1,)
    #print("Predicted datas:")
    #print(next_prediction)

    # To get activation
    activations = get_activations(sliced)
    activation_list.append( activations )
    prediction_list.append(next_prediction)
    #print("Activation Data")
    #print(activation_list)

    sliced = next_prediction

numpy_activation_array = np.asarray(activation_list)
numpy_prediction_array = np.asarray(prediction_list)
reshpaped_activation_array = np.reshape(numpy_activation_array, (200,150))
reshpaped_prediction_array = np.reshape(numpy_prediction_array, (200,90))

output_prediction_data = pd.DataFrame(reshpaped_prediction_array)
output_prediction_data.to_excel(write_predict_data, sheet_name='Sheet1')

output_activation_data = pd.DataFrame(reshpaped_activation_array)
output_activation_data.to_excel(write_activations_data, sheet_name='Sheet1')
write_predict_data.save()
write_activations_data.save()


write_prediction_after_delete = pd.ExcelWriter("/home/rianzaman/Downloads/prediction_table_after_delete_test.xlsx",engine="xlsxwriter")
##Deleting a specific column;
#column = 90
#for l in range(0,column):
test_array_container = np.delete(reshpaped_prediction_array,[0,3,6,9,12,15,18,21,24,27,30,33,36,39,42,45,48,51,54,57,60,
                                                             63,66,69,72,75,78,81,84,87,90], axis = 1)
 #   l+=3

output_prediction_after_delete_data = pd.DataFrame(test_array_container)
output_prediction_after_delete_data.to_excel(write_prediction_after_delete, sheet_name='Sheet1')

temp_x_list = []
temp_y_list = []
#Getting Datas from an array
write_data_of_xy = pd.ExcelWriter("/home/rianzaman/Downloads/xy_data_test.xlsx",engine="xlsxwriter")
new_row = 200
new_column = 60
excel_col_x = 0
excel_col_y = 0
for i in range(0,new_row):
    if(i>0):
        print("Started To add...", i)
        excel_col_x += 2
        excel_col_y += 2
    print("Doing something .....")
    temp_x_list = []
    temp_y_list = []
    for j in range(0,new_column):
        if(j%2 == 0):
            print("Working on X...")
            temp_x_list.append(test_array_container[i][j])
        else:
            print("Working on Y...")
            temp_y_list.append(test_array_container[i][j])
        j+=1
    i+=1
    output_x = pd.DataFrame(temp_x_list)
    output_x.to_excel(write_data_of_xy, sheet_name='Sheet1',startcol=excel_col_x)
    output_y = pd.DataFrame(temp_y_list)
    output_y.to_excel(write_data_of_xy, sheet_name='Sheet2',startcol=excel_col_y)
    excel_col_x +=2
    excel_col_y +=2





print("Shape of the converted Activation List:")
print(reshpaped_activation_array)




#np.savetxt("/home/rianzaman/Downloads/activation_data_file_2.csv", activation_array.flatten(), delimiter=",",)
#activaiton_array.tofile('/home/rianzaman/Downloads/activation_data_file_1.csv',sep=',',format='%10.5f')
#activation_file = open('/home/rianzaman/Downloads/activation_data_file_1.csv', 'wb')


#writer = csv.writer(activation_file)
#writer.writerow(['label1',])
#for values in activation_list:
    #writer.writerow(values)

#activation_file.close()

#score = my_model.evaluate(X_test, Y_test, show_accuracy=False, verbose=0)
#print('Test score:', score[0])
#print('Test accuracy:', score[1])
