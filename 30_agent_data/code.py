from keras.models import Sequential
from keras.layers.core import Dense,Activation
import pandas as pd
import theano
import numpy as np
import xlsxwriter
#from keras.utils.visualize_util import plot


my_model = Sequential()
print('Loading data...')
training_data_x = pd.read_excel("/home/rianzaman/Downloads/New_Datas/30_agents/2_hidden_layers/inputs/training_data_for_x.xlsx", header = None)
training_data_y = pd.read_excel("/home/rianzaman/Downloads/New_Datas/30_agents/2_hidden_layers/inputs/training_data_for_y.xlsx", header = None)
test_data_x = pd.read_excel("/home/rianzaman/Downloads/New_Datas/30_agents/2_hidden_layers/inputs/test_data_for_x.xlsx", header = None)
test_data_y = pd.read_excel("/home/rianzaman/Downloads/New_Datas/30_agents/2_hidden_layers/inputs/test_data_for_y.xlsx", header = None)

X_train = training_data_x.as_matrix(columns=None)
Y_train = training_data_y.as_matrix(columns=None)

X_test = test_data_x.as_matrix(columns=None)
Y_test = test_data_y.as_matrix(columns=None)
'''
################Controlled Data ####################
controlled_data = pd.read_excel("/home/rianzaman/Downloads/New_Datas/30_agents/2_hidden_layers/inputs/controlled_data.xlsx", header = None)
controlled_data_set = controlled_data.as_matrix(columns=None)
################Controlled Data ####################
'''
size_of_batch = 100
number_of_epoch =200

print('Building model...')
########################################################################
my_model.add(Dense(150, input_shape=(90,),))
my_model.add(Dense(150,))
my_model.add(Dense(90,))
########################################################################
print('Configuring the model...')
my_model.compile(loss = 'mean_absolute_error', optimizer = 'adadelta')


print('Training the network...')
my_model.fit(X_train, Y_train,batch_size=size_of_batch, nb_epoch=number_of_epoch, show_accuracy=False, verbose=2,
         validation_data=(X_test, Y_test))

print('Saving weights...')
my_model.save_weights( '/home/rianzaman/Downloads/New_Datas/30_agents/2_hidden_layers/controlled_non_lin_out/layer_weights.h5')
'''
print('Loading Weights...')
my_model.load_weights( '/home/rianzaman/Downloads/New_Datas/30_agents/2_hidden_layers/controlled_non_lin_out/layer_weights.h5')
'''
#Weight Visualization
print("Printing Weights of layer")
weights_1 = my_model.layers[0].get_weights()
weights_2 = my_model.layers[1].get_weights()
weights_3 = my_model.layers[2].get_weights()
reshpaped_weights = np.reshape(weights_1[0], (90,150))
write_weights = pd.ExcelWriter("/home/rianzaman/Downloads/New_Datas/30_agents/2_hidden_layers/controlled_non_lin_out/weights_1.xlsx",engine="xlsxwriter")
weights_output = pd.DataFrame(reshpaped_weights)
weights_output.to_excel(write_weights, sheet_name='Sheet1',index = False)

reshpaped_weights_1 = np.reshape(weights_2[0], (150,150))
write_weights_1 = pd.ExcelWriter("/home/rianzaman/Downloads/New_Datas/30_agents/2_hidden_layers/controlled_non_lin_out/weights_2.xlsx",engine="xlsxwriter")
weights_output_1 = pd.DataFrame(reshpaped_weights_1)
weights_output_1.to_excel(write_weights_1, sheet_name='Sheet1',index = False)

reshpaped_weights_2 = np.reshape(weights_3[0], (150,90))
write_weights_2 = pd.ExcelWriter("/home/rianzaman/Downloads/New_Datas/30_agents/2_hidden_layers/controlled_non_lin_out/weights_3.xlsx",engine="xlsxwriter")
weights_output_2 = pd.DataFrame(reshpaped_weights_2)
weights_output_2.to_excel(write_weights_2, sheet_name='Sheet1',index = False)

#Weight visualization ends.

#Bias weights starts:

bias_1 = np.reshape(weights_1[1], (1,150))
bias_2 = np.reshape(weights_2[1], (1,150))
bias_3 = np.reshape(weights_3[1], (1,90))

write_bias_1 = pd.ExcelWriter("/home/rianzaman/Downloads/New_Datas/30_agents/2_hidden_layers/controlled_non_lin_out/bias_1.xlsx",engine="xlsxwriter")
bias_1_output = pd.DataFrame(bias_1)
bias_1_output.to_excel(write_bias_1, sheet_name='Sheet1',index = False)

write_bias_2 = pd.ExcelWriter("/home/rianzaman/Downloads/New_Datas/30_agents/2_hidden_layers/controlled_non_lin_out/bias_2.xlsx",engine="xlsxwriter")
bias_2_output = pd.DataFrame(bias_2)
bias_2_output.to_excel(write_bias_2, sheet_name='Sheet1',index = False)

write_bias_3 = pd.ExcelWriter("/home/rianzaman/Downloads/New_Datas/30_agents/2_hidden_layers/controlled_non_lin_out/bias_3.xlsx",engine="xlsxwriter")
bias_3_output = pd.DataFrame(bias_3)
bias_3_output.to_excel(write_bias_3, sheet_name='Sheet1',index = False)
#Bias weights ends here.


get_activations = theano.function([my_model.layers[0].input], my_model.layers[0].output,
                                  allow_input_downcast=True)
#Enable for controlled dataset.
#sliced = controlled_data_set[0:1,:]
sliced = X_test[0:1,:]

activation_list = []
prediction_list = []
prediction_list.append(sliced)
for i in range(0, 200):
    print("Predicting ...")
    next_prediction = my_model.predict(sliced, 1,)
    # To get activation
    activations = get_activations(sliced)
    activation_list.append( activations )
    prediction_list.append(next_prediction)
    sliced = next_prediction

numpy_activation_array = np.asarray(activation_list)
numpy_prediction_array = np.asarray(prediction_list)
reshpaped_activation_array = np.reshape(numpy_activation_array, (200,150))
reshpaped_prediction_array = np.reshape(numpy_prediction_array, (201,90))

write_predict_data = pd.ExcelWriter("/home/rianzaman/Downloads/New_Datas/30_agents/2_hidden_layers/controlled_non_lin_out/predicted_data.xlsx",engine="xlsxwriter")
output_prediction_data = pd.DataFrame(reshpaped_prediction_array)
output_prediction_data.to_excel(write_predict_data, sheet_name='Sheet1',index = False)

write_activations_data = pd.ExcelWriter("/home/rianzaman/Downloads/New_Datas/30_agents/2_hidden_layers/controlled_non_lin_out/activition_of_1st_layer.xlsx",engine="xlsxwriter")
output_activation_data = pd.DataFrame(reshpaped_activation_array)
output_activation_data.to_excel(write_activations_data, sheet_name='Sheet1',index = False)

write_predict_data.save()
write_activations_data.save()

#Deleting a specific column:
number_of_column_to_delete = []
predict_arr_col = 90
for cl in range(predict_arr_col):
    if(cl%3 is 0):
        number_of_column_to_delete.append(cl)
test_array_container = np.delete(reshpaped_prediction_array,number_of_column_to_delete, axis = 1)

write_prediction_after_delete = pd.ExcelWriter("/home/rianzaman/Downloads/New_Datas/30_agents/2_hidden_layers/controlled_non_lin_out/prediction_table_after_delete.xlsx",engine="xlsxwriter")
output_prediction_after_delete_data = pd.DataFrame(test_array_container)
output_prediction_after_delete_data.to_excel(write_prediction_after_delete, sheet_name='Sheet1',index = False)
#Deleting a specific column code ends here.

#Getting Datas from an array
temp_x_list = []
temp_y_list = []

write_data_of_xy = pd.ExcelWriter("/home/rianzaman/Downloads/New_Datas/30_agents/2_hidden_layers/controlled_non_lin_out/xy_data.xlsx",engine="xlsxwriter")
new_rows = 200
new_columns = 60
excel_col_x = 1
excel_col_y = 2
for i in range(0 , new_rows):
    #print("Doing something .....")
    temp_x_list = []
    temp_y_list = []
    for j in range(0,new_columns):
        if(j%2 == 0):
            #print("Working on X...")
            temp_x_list.append(test_array_container[i][j])
        else:
            #print("Working on Y...")
            temp_y_list.append(test_array_container[i][j])
        j+=1
    i+=1
    output_x = pd.DataFrame(temp_x_list)
    output_x.to_excel(write_data_of_xy, sheet_name='Sheet1',startcol=excel_col_x ,index = False)
    output_y = pd.DataFrame(temp_y_list)
    output_y.to_excel(write_data_of_xy, sheet_name='Sheet1',startcol=excel_col_y, index = False)
    excel_col_x +=3
    excel_col_y +=3


#To visualize the model:
#plot(my_model, to_file='/home/rianzaman/Downloads/New_Datas/10_agents/2_hidden_layers/outputs/my_model.png')


score = my_model.evaluate(X_test, Y_test, show_accuracy=False, verbose=0)
#print('Test score:', score[0])
#print('Test accuracy:', score[1])
